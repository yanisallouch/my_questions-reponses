import 'package:questionreponses/models/question_model.dart';

List<QuestionModel> getQuestions() {
  List<QuestionModel> questions = [];
  QuestionModel questionModel = new QuestionModel(answer: '', question: '');

  //1
  questionModel.setQuestion("Un bébé a plus d'os qu'un adulte?");
  questionModel.setAnswer("True");
  questionModel.setImageUrl(
      "https://images.pexels.com/photos/789786/pexels-photo-789786.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940");
  questions.add(questionModel);
  questionModel = new QuestionModel(answer: '', question: '');

  //2
  questionModel.setQuestion("Est-ce que les requins peuvent cligner des yeux?");
  questionModel.setAnswer("True");
  questionModel.setImageUrl(
      "https://images.pexels.com/photos/8065516/pexels-photo-8065516.jpeg?cs=srgb&dl=pexels-cliff-simon-8065516.jpg&fm=jpg&auto=compress&cs=tinysrgb&dpr=2&h=650&w=940");
  questions.add(questionModel);

  questionModel = new QuestionModel(question: '', answer: '');

  //3
  questionModel
      .setQuestion("Le dollar américan (US) est imprimé a partir de fibre ?");
  questionModel.setAnswer("False");
  questionModel.setImageUrl(
      "https://images.pexels.com/photos/164661/pexels-photo-164661.jpeg?cs=srgb&dl=pexels-pixabay-164661.jpg&fm=jpg");
  questions.add(questionModel);

  questionModel = new QuestionModel(question: '', answer: '');

  //4
  questionModel
      .setQuestion("Les Sandwich, ont été nommé d'apres une personne ?");
  questionModel.setAnswer("True");
  questionModel.setImageUrl(
      "https://images.pexels.com/photos/1647163/pexels-photo-1647163.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500");
  questions.add(questionModel);

  questionModel = new QuestionModel(question: '', answer: '');

  //5
  questionModel.setQuestion("Les tigres ont des rayures?");
  questionModel.setAnswer("True");
  questionModel.setImageUrl(
      "https://images.pexels.com/photos/302304/pexels-photo-302304.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500");
  questions.add(questionModel);

  questionModel = new QuestionModel(question: '', answer: '');

  //6
  questionModel.setQuestion("Les autriches ont les plus gros yeux au monde ?");
  questionModel.setAnswer("False");
  questionModel.setImageUrl(
      "https://images.pexels.com/photos/2233442/pexels-photo-2233442.jpeg?cs=srgb&dl=pexels-markus-distelrath-2233442.jpg&fm=jpg");
  questions.add(questionModel);

  questionModel = new QuestionModel(question: '', answer: '');

  //7
  questionModel
      .setQuestion("Environ 16 milliards d'emails sont envoyés chaque jour ?");
  questionModel.setAnswer("False");
  questionModel.setImageUrl(
      "https://images.pexels.com/photos/5605061/pexels-photo-5605061.jpeg?cs=srgb&dl=pexels-maksim-goncharenok-5605061.jpg&fm=jpg");
  questions.add(questionModel);

  questionModel = new QuestionModel(question: '', answer: '');

  //8
  questionModel.setQuestion("Le groupe sanguin des Gorilles est de type B?");
  questionModel.setAnswer("True");
  questionModel.setImageUrl(
      "https://images.pexels.com/photos/2657973/pexels-photo-2657973.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500");
  questions.add(questionModel);

  questionModel = new QuestionModel(question: '', answer: '');

  return questions;
}
